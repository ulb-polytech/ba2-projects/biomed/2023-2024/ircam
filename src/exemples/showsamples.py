#!/usr/bin/env python
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from argparse import ArgumentParser

#arguments parser
argparse = ArgumentParser()
argparse.add_argument('-f','--file', type=str, help='Input filename', default="samples.txt")
argparse.add_argument('-o','--output', type=str, help='Output image filename with extension for the plot, e.g. samples.png', default="")
args = argparse.parse_args()

#load samples from file
samples = np.loadtxt(args.file,delimiter=" ") 

ts = samples[:,0]  #timestamp is the first column
nbs = samples[:,1] #samples number is the second column when grab with "G"
analog = samples[:,2] #analog input is the third column when grab with "G"
di = samples[:,3:] #other columns are digital input with 0 or 1

plt.plot(ts,analog,'r',label='Analog values from ADC')
plt.legend(loc='upper right', shadow=True, fontsize='x-large')
plt.xlabel('Timestamp (sec)')
plt.ylabel('Analog input')
if len(args.output):
    plt.savefig(args.output) #save output figure in a file
plt.show()