# Description

Répertoire contenant l'ensemble des programmes de votre microcontrôleur compatible "Arduino" avec les librairies utiles à la réalisation du projet.

Veuillez faire pointer le chemin `Emplacement de croquis` de vos préférences de l'Arduino IDE vers celui-ci.

Alternativement, vous pouvez également créer un noubeau répositoire dans votre groupe _BIOMEDx_  pour chaque programme Arduino que vous créerez.

Dans ce répertoire, nous avons ajouté un exemple de programme Arduino nommé [`PySerialCOM`](./PySerialCOM/PySerialCOM.ino) sur lequel vous pouvez vous baser pour la communication série entre un microcontrôleur et un PC. Veuillez lire le [README de ce dossier](./PySerialCOM/README.md) pour plus d'informations sur ce programme.



