#ifndef _PARSERS_
#define _PARSERS_

#ifndef DBGPARSER
   #define printdbg(...)  
#else
   #define printdbg(...) {Serial.print("[");Serial.print(__FILE__);Serial.print("::");Serial.print(__FUNCTION__); Serial.print("(): "); Serial.print(__LINE__); Serial.print(" ] ");Serial.println(__VA_ARGS__);}
#endif

int parseInt(char const *s) {
     if ( s == NULL || *s == '\0' ) { 
      printdbg("null or empty string argument for int conversion");
      return 0;
     }
     while (*s==' ') ++s;
     bool negate = (s[0] == '-');
     if ( *s == '+' || *s == '-' ) ++s;
     if ( *s == '\0') {
        printdbg("sign character only.");
        return 0;
     }
     int result = 0;
     while(*s) {
          if ( *s < '0' || *s > '9' ) {
              if (*s != '\n' && *s != '\r') printdbg(String("invalid input char for int conversion : ")+String(*s));              
          } else result = result * 10  + *s - '0';  
          ++s;
     }
     return negate ? -result : result;
}

double parseDouble(char const *s) {
  if ( s == NULL || *s == '\0' ) { 
      printdbg("null or empty string argument for double conversion");
      return 0;
  }
  while (*s==' ') ++s;
  bool negate = (s[0] == '-');
  bool decimal = false;  
  double result=0, fact=0.1;
  while (*s) {
    if (*s >= '0' && *s <= '9') {
      if (!decimal) result = result * 10 + *s - '0';
      else {
        result += ((double)(*s - '0') * fact);
        fact *= 0.1;
      }
    } 
    else { 
      if (*s == '.' || *s == ',') {
          decimal = true; //check if a decimal part exists using . or ,
      }
      else { 
        if (*s == 'e' || *s == 'E') {  //check exponential notation
          int exp=parseInt(++s); //parse the exponent part as integer
          if (exp>0) {
            for (int i=0;i<exp;i++) result=result*10; //positive exponent
          } else {
            for (int i=0;i<-exp;i++) result=result/10; //negative exponent
          }
          break;
        }
        else { 
          if (*s != '\n' && *s != '\r') printdbg(String("invalid input char for double conversion : ")+String(*s));          
        }
      }      
    }    
    ++s;
  }
  return negate ? -result : result;
}

void parseString(char const *s, char *str) {
  while (*s==' ') ++s;
  while (*s && *s != '\n' && *s != '\r') {
    *str=*s;
    ++s;
    ++str;
  }
  *str=0;
}

#endif
