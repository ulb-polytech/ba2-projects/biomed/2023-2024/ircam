# Précision du capteur

## Choix du capteur

Ce capteur de température sans contact a été choisi sur base des critères suivants :

- Critère 1
- Critère 2

Ce capteur a une précision de 1-2° comme mentionné dans la [datasheet](http://....) et nous allons le vérifier (ou pas) dans la suite de ce document.


## Expérience(s) de mesure de la précision

### Description de l'(des) expérience(s)

1. Le capteur est ...
2. Branchement du capteur au microcontrôleur XXX sur la patte x ...
3. ...

Belles photos/schémas/vidéos de l'(des) expérience(s)

### Conditions de l'expérience

Différentes conditions (cf. température ambiante, ...)

### Matériel nécessaire

Décrire tous le matériel nécessaire à l'expérience : capteur(s), microncontrôleur, ... 

### Paramètres de l'expérience

Le capteur a été réglé de cette manière avant le début de l'expérience ...

### Reproductibilité

Nous avons reproduit cette expérience plusieurs fois dans
- Les mêmes conditions, avec les même paramètres, etc.
- Ou pas (à _détailler_) 

Nous avons obtenu les résultats présentés dans la section suivante.

### Résultats 

#### Expérience 1

Les graphiques ci-dessous montrent la précision du capteur dans les conditions ... avec les paramètres ...

#### Expérience 2

Idem expérience 1

#### Expérience 3

Idem expérience 1 mais conditions/paramètres suivants différents


## Amélioration de la précision (recalibration)

**Section optionnelle !**

Comme vu lors de(s) (l') expérience(s) précédente(s), la précision du capteur n'est pas suffisante. Toutefois, nous pouvons traiter les données en effectuant une recalibration à condition que ...

En résumé, afin d'améliorer la précision, nous devons effectuer les étapes suivantes lors du traitements des données :
1. Etape 1
2. ...


## Conclusion

Comme le montre les résultats, le capteur X a bien la précision spécifiée par sa datasheet.

**Sinon, expliquer-le ici** (discussion/hypothèses admises mais qui doivent être plausibles) !





